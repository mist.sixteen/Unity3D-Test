using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabButton : MonoBehaviour
{
    [SerializeField]
    private RectTransform rectTransform;

    [SerializeField]
    private TabPageButtons tabPages;

    [SerializeField]
    private Button button;

    [SerializeField]
    private Image image;

    [SerializeField]
    private Sprite spriteOnSelect, spriteOffSelect;

    private Vector2 sizeOnSelect = new Vector2(70, 50);
    private Vector2 sizeOnDeselect = new Vector2(55, 50);

    // Start is called before the first frame update
    private void Awake()
    {
        this.button.onClick.AddListener(() =>
        {
            tabPages?.OnTabEnter(this);
        });
    }

    public void OnSelect()
    {
        image.color = Color.blue;
        rectTransform.sizeDelta = sizeOnSelect;
        
        //image.sprite = spriteOnSelect;
    }

    public void OnDeselect()
    {
        image.color = Color.white;
        rectTransform.sizeDelta = sizeOnDeselect;

        //image.sprite = spriteOffSelect;
    }

}
