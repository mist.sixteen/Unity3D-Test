using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TabPageButtons : MonoBehaviour
{
    private TabButton[] tabButtons;
    private TabPage[] tabPages;

    private void Awake()
    {
        tabButtons = this.gameObject.GetComponentsInChildren<TabButton>(true);
        tabPages = this.gameObject.GetComponentsInChildren<TabPage>(true);

        this.OnTabEnter(tabButtons[0]);
    }

    public void OnTabEnter(TabButton button)
    {
        var targetIdx = -1;

        for (int i = 0; i < tabButtons.Length; i++)
        {
            if (button == tabButtons[i])
            {
                tabButtons[i].OnSelect();
                targetIdx = i;
            }
            else
                tabButtons[i].OnDeselect();
        }

        for (int i = 0; i < tabPages.Length; i++)
        {
            if (i == targetIdx)
                tabPages[i].gameObject.SetActive(true);
            else
                tabPages[i].gameObject.SetActive(false);
        }
    }
}