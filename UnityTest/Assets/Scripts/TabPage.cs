using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TabPage : MonoBehaviour
{
    public UnityAction onEnabled;
    public UnityAction onDisabled;

    // Start is called before the first frame update
    void Start()
    {
        onEnabled += (() => { Debug.Log("Event OnEnabled"); });
        onDisabled += (() => { Debug.Log("Event OnDisabled"); });
    }

    private void OnEnable()
    {
        onEnabled?.Invoke();
    }
    private void OnDisable()
    {
        onDisabled?.Invoke();
    }
}
